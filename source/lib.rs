/*
	This Source Code Form is subject to the terms of the Mozilla Public
	License, v. 2.0. If a copy of the MPL was not distributed with this
	file, You can obtain one at http://mozilla.org/MPL/2.0/.
*/

#![no_std]
#![no_main]

#![feature(error_in_core)]

#![allow(special_module_name)]

pub mod io {
	pub mod serial;
	pub use serial::SerialInterface;

	pub mod ports;
}

mod main;

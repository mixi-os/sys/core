/*
	This Source Code Form is subject to the terms of the Mozilla Public
	License, v. 2.0. If a copy of the MPL was not distributed with this
	file, You can obtain one at http://mozilla.org/MPL/2.0/.
*/

use core::arch::asm;
use core::mem::size_of;

use num_traits::{Unsigned, FromPrimitive};

pub unsafe fn input<T>(port: u16) -> Option<T>
where
	T: Unsigned + FromPrimitive
{
	assert!(size_of::<T>() <= 4);

	match size_of::<T>() {
		1 => {
			let result: u8;

			asm!(
				"inb %dx, %al",
				in("dx") port,
				out("al") result,
				options(att_syntax)
			);

			return T::from_u8(result);
		}
		2 => {
			let result: u16;

			asm!(
				"inw %dx, %ax",
				in("dx") port,
				out("ax") result,
				options(att_syntax)
			);

			return T::from_u16(result);
		}
		4 => {
			let result: u32;

			asm!(
				"inl %dx, %eax",
				in("dx") port,
				out("eax") result,
				options(att_syntax)
			);

			return T::from_u32(result);
		}
		_ => unreachable!()
	};
}

pub unsafe fn output<T>(port: u16, data: T)
where
	T: Unsigned + Into<u32>
{
	assert!(size_of::<T>() <= 4);

	let data = data.into();

	match size_of::<T>() {
		1 => asm!(
			"outb %al, %dx",
			in("al") data as u8,
			in("dx") port,
			options(att_syntax)
		),
		2 => asm!(
			"outw %ax, %dx",
			in("ax") data as u16,
			in("dx") port,
			options(att_syntax)
		),
		4 => asm!(
			"outl %eax, %dx",
			in("eax") data as u32,
			in("dx") port,
			options(att_syntax)
		),
		_ => unreachable!()
	};
}

/*
	This Source Code Form is subject to the terms of the Mozilla Public
	License, v. 2.0. If a copy of the MPL was not distributed with this
	file, You can obtain one at http://mozilla.org/MPL/2.0/.
*/

use crate::io::ports::*;

#[derive(Clone, Copy)]
pub enum Port {
	COM1 = 0x3f8,
	COM2 = 0x2f8,
	COM3 = 0x3e8,
	COM4 = 0x2e8,
	COM5 = 0x5f8,
	COM6 = 0x4f8,
	COM7 = 0x5e8,
	COM8 = 0x4e8,
}

pub struct SerialInterface {
	port: Port,
	baudrate: u32,
}

impl SerialInterface {
	const BASE_BAUD_RATE: u32 = 115200;

	pub fn new(port: Port) -> Self {
		return Self {
			port: port,
			baudrate: 0,
		}
	}

	// TODO serial connection options
	pub fn connect(&mut self, baudrate: u32) -> Result<(), ()> {
		unsafe {
			let port: u16 = self.port as u16;

			output::<u8>(port + 1, 0x00); // disable interrupts
			output::<u8>(port + 3, 0x80); // enable DLAB
			output::<u8>(port + 0, (Self::BASE_BAUD_RATE / baudrate) as u8); // set baud rate divisor
			output::<u8>(port + 1, 0x00);
			output::<u8>(port + 3, 0x03); // 8 bits, no parity, one stop bit
			output::<u8>(port + 2, 0xC7); // FIFO
			output::<u8>(port + 4, 0x0B); // IRQs enabledm RTS/DSR set
			output::<u8>(port + 4, 0x1E); // loopback mode
			output::<u8>(port + 0, 0xAE); // send test byte

			// TODO return actual error on failure
			return input::<u8>(port).map_or(Err(()), |result| {
				if result != 0xAE {
					return Err(());
				}

				output::<u8>(port + 4, 0x0F); // normal operation mode

				self.baudrate = baudrate;

				return Ok(());
			});
		}
	}

	/// is there any data available to read?
	pub fn available(&self) -> bool {
		assert_ne!(self.baudrate, 0);

		unsafe {
			return input::<u8>((self.port as u16) + 5).map_or(false, |result| {
				return (result & 1) != 0;
			});
		}
	}

	/// is the write queue empty?
	pub fn queue_empty(&self) -> bool {
		assert_ne!(self.baudrate, 0);

		unsafe {
			return input::<u8>((self.port as u16) + 5).map_or(false, |result| {
				return (result & 0x20) != 0;
			});
		}
	}

	pub fn read(&self, blocking: bool) -> Option<u8> {
		assert_ne!(self.baudrate, 0);

		unsafe {
			while !blocking && !self.available() { }
			return input::<u8>(self.port as u16);
		}
	}

	pub fn write(&self, value: u8, blocking: bool) {
		assert_ne!(self.baudrate, 0);

		unsafe {
			while !blocking && !self.queue_empty() { }
			return output::<u8>(self.port as u16, value);
		}
	}
}

/*
	This Source Code Form is subject to the terms of the Mozilla Public
	License, v. 2.0. If a copy of the MPL was not distributed with this
	file, You can obtain one at http://mozilla.org/MPL/2.0/.
*/

use core::panic::PanicInfo;
use crate::io::{serial, SerialInterface};

#[panic_handler]
fn panic(_info: &PanicInfo) -> ! {
	loop {}
}

#[no_mangle]
pub extern "C" fn kmain() {
	unsafe {
		let vga = 0xC03FF000 as *mut u64;
		*vga = 0x2f592f412f4b2f4f;
	}

	let mut si: SerialInterface = SerialInterface::new(serial::Port::COM1);
	si.connect(115200).expect("didn't connect to serial");

	si.write('a' as u8, true);
	si.write('\n' as u8, true);

	for x in "hello".chars() {
		si.write(x as u8, true);
	}
}

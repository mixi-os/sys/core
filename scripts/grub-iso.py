#!/usr/bin/env python
import subprocess
import argparse
import os, os.path
import glob
import sys
import tempfile
from shutil import which, copyfile

assert "CARGO_MAKE_CURRENT_TASK_INITIAL_MAKEFILE_DIRECTORY" in os.environ
assert "CARGO_MAKE_CRATE_NAME" in os.environ

MKRESCUE = None

if which("grub2-mkrescue") != None:
	MKRESCUE = "grub2-mkrescue"
elif which("grub-mkrescue") != None:
	MKRESCUE = "grub-mkrescue"

assert MKRESCUE != None

def find_latest(pattern):
	files = glob.glob(pattern, recursive=True)
	
	if not files:
		return None
	
	files.sort(key=lambda x: os.path.getmtime(x), reverse=True)
	
	latest_file = files[0]
	return latest_file

PROJECT_DIR = os.environ["CARGO_MAKE_CURRENT_TASK_INITIAL_MAKEFILE_DIRECTORY"]
PROJECT_NAME = os.environ["CARGO_MAKE_CRATE_NAME"]

GRUB_CFG = """
set timeout=3
set default=0

menuentry \"live kernel\" {
	multiboot2 /kernel
	boot
}
"""

if __name__ == "__main__":
	parser = argparse.ArgumentParser(description="creates a bootable grub iso for the kernel.")
	parser.add_argument("-k", "--kernel", required=False, help="the kernel binary to package in the iso")

	args = parser.parse_args()

	if args.kernel is None:
		args.kernel = find_latest(f"{PROJECT_DIR}/build/**/{PROJECT_NAME}.bin")

	assert os.path.isfile(args.kernel)

	output = f"{os.path.dirname(args.kernel)}/{PROJECT_NAME}.iso"
	isoroot = tempfile.TemporaryDirectory()
	
	print("|| iso root:", isoroot.name)

	os.makedirs(f"{isoroot.name}/boot/grub")
	
	with open(f"{isoroot.name}/boot/grub/grub.cfg", "w+") as f:
		f.write(GRUB_CFG)
	
	copyfile(args.kernel, f"{isoroot.name}/kernel")

	p = subprocess.Popen(f"{MKRESCUE} -o {output} {isoroot.name}",
		shell = True)
	print(">>", p.args)

	if p.wait() != 0:
		isoroot.cleanup()
		sys.exit(p.returncode)
	
	isoroot.cleanup()

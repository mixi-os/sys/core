#!/usr/bin/env python
import subprocess
import argparse
import os, os.path
import glob
import sys

assert "CARGO_MAKE_CURRENT_TASK_INITIAL_MAKEFILE_DIRECTORY" in os.environ
assert "CARGO_MAKE_CRATE_NAME" in os.environ

def find_latest_directory(pattern):
	files = glob.glob(pattern, recursive=True)
	
	if not files:
		return None
	
	files.sort(key=lambda x: os.path.getmtime(x), reverse=True)
	
	latest_file = files[0]
	latest_directory = os.path.dirname(latest_file)
	
	return latest_directory

PROJECT_DIR = os.environ["CARGO_MAKE_CURRENT_TASK_INITIAL_MAKEFILE_DIRECTORY"]
PROJECT_NAME = os.environ["CARGO_MAKE_CRATE_NAME"]

if __name__ == "__main__":
	parser = argparse.ArgumentParser(description="builds an architectural target with cargo and then calls its build script.")
	parser.add_argument("-a", "--arch", required=True, help="target architecture to build for")

	args, cargo_args = parser.parse_known_args()

	target = f"{PROJECT_DIR}/arch/{args.arch}.json"
	target_dir = f"{PROJECT_DIR}/arch/{args.arch}"

	assert os.path.isfile(target)
	assert os.path.isdir(target_dir)

	if not os.path.isfile(target):
		print(f"!! fatal: architecture not supported: {args.arch}")
		sys.exit(2)
	
	p = subprocess.Popen(f"cargo build --target \"{target}\" {' '.join(cargo_args)}",
		shell = True, cwd=PROJECT_DIR, env=os.environ)
	print(">>", p.args)
	
	if p.wait() != 0:
		sys.exit(p.returncode)
	
	build_dir = find_latest_directory(f"{PROJECT_DIR}/build/**/lib{PROJECT_NAME}.a")

	assert build_dir != None

	print("|| found latest build directory:", build_dir)

	e = os.environ.copy()
	e["PROJECT_NAME"] = PROJECT_NAME
	e["PROJECT_DIR"] = PROJECT_DIR
	e["BUILD_DIR"] = build_dir

	print("|| calling target build script:")

	p = subprocess.Popen(f"python3 build.py",
		shell = True, cwd=target_dir, env=e)
	
	if p.wait() != 0:
		sys.exit(p.returncode)

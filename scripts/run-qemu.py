#!/usr/bin/env python
import subprocess
import argparse
import os, os.path
import glob
import sys
import tempfile
from shutil import which, copyfile

assert "CARGO_MAKE_CURRENT_TASK_INITIAL_MAKEFILE_DIRECTORY" in os.environ
assert "CARGO_MAKE_CRATE_NAME" in os.environ

def find_latest(pattern):
	files = glob.glob(pattern, recursive=True)
	
	if not files:
		return None
	
	files.sort(key=lambda x: os.path.getmtime(x), reverse=True)
	
	latest_file = files[0]
	return latest_file

PROJECT_DIR = os.environ["CARGO_MAKE_CURRENT_TASK_INITIAL_MAKEFILE_DIRECTORY"]
PROJECT_NAME = os.environ["CARGO_MAKE_CRATE_NAME"]

if __name__ == "__main__":
	parser = argparse.ArgumentParser(description="creates a bootable grub iso for the kernel.")
	parser.add_argument("-a", "--arch", required=True, help="target architecture to run")
	parser.add_argument("-i", "--image", required=False, help="the iso image to run in qemu")

	args = parser.parse_args()

	qemu = "qemu-system-"

	match args.arch:
		case "x86":
			qemu = qemu + "i386"
		case _:
			qemu = qemu + args.arch

	if args.image is None:
		pattern = f"{PROJECT_DIR}/build/{args.arch}/**/{PROJECT_NAME}.iso"
		print("|| no image provided, looking with pattern:", pattern)
		
		args.image = find_latest(pattern)

	#assert os.path.isfile(args.image)
	#assert which("qemu-system-i386") != None

	if args.image == None or not os.path.isfile(args.image):
		print("!! no iso image found:", args.image)
		sys.exit(1)

	if which(qemu) == None:
		print("!! qemu not installed:", qemu)
		sys.exit(2)

	p = subprocess.Popen(f"{qemu} \
		-s -S \
		-m 512 \
		-d int \
		-chardev stdio,id=char0,logfile=serial.log,signal=off \
		-serial chardev:char0 \
		-cdrom {args.image}",
		shell = True, )
	print(">>", p.args)
	
	if p.wait() != 0:
		sys.exit(p.returncode)

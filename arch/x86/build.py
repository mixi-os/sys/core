#!/usr/bin/env python
import subprocess
import sys
import os

PROJECT_NAME = os.environ["PROJECT_NAME"]
BUILD_DIR = os.environ["BUILD_DIR"]

p = subprocess.Popen(f"make \"{BUILD_DIR}/{PROJECT_NAME}.bin\"",
	shell=True, env=os.environ)

if p.wait() != 0:
	sys.exit(p.returncode)

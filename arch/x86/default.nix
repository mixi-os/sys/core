#!/usr/bin/env nix-shell
let
	pkgs = import <nixpkgs> { };
	fenix = import (fetchTarball "https://github.com/nix-community/fenix/archive/5b116a689c22ed2495c2b0f857539519a2951ce2.tar.gz") { };
	#pkgs  = import (fetchTarball "https://github.com/nixos/nixpkgs/archive/1d07743667b6ad1e149a539d3d4ead66d31da901.tar.gz") { };
	#fenix = import (fetchTarball "https://github.com/nix-community/fenix/archive/4a2b238e9de609f5f04a0bd883771baa8a0a7165.tar.gz") { };
in
pkgs.mkShell {
	nativeBuildInputs = [
		(fenix.complete.withComponents [
			"cargo"
			"clippy"
			"rust-src"
			"rustc-unwrapped"
			"rustfmt"
		])
	] ++ (with pkgs.buildPackages; [
		gdb
		
		lld
		nasm

		grub2
		xorriso

		buildPackages.qemu
	]);
}
